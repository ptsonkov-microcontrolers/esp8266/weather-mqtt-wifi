#include <Adafruit_BME280.h>
#include <Adafruit_Sensor.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include "code_secrets.h"

// Set WiFi credentials
#define wifi_ssid SECRET_WIFI_SSID
#define wifi_password SECRET_WIFI_PASS

// Set MQTT endpoint
#define mqtt_server SECRET_MQTT_SERVER
#define mqtt_port SECRET_MQTT_PORT
#define mqtt_user SECRET_MQTT_USER
#define mqtt_pass SECRET_MQTT_PASS

#define sensor_name "ranchnet"
#define sensor_location "out"
#define topic_temperature sensor_name "/" sensor_location "/temperature"
#define topic_humidity sensor_name "/" sensor_location "/humidity"
#define topic_pressure sensor_name "/" sensor_location "/pressure"

#define on_led D4
#define red_led D5
#define yellow_led D6
#define green_led D7

WiFiClient espClient;
PubSubClient mqttClient(espClient);
Adafruit_BME280 bme;

int deep_sleep_timer = 900;
int no_wifi_sleep = 10;
unsigned long connect_timeout = 30;
unsigned long timeNow = 0;
unsigned long timeDelta = 0;

float temperature, humidity, pressure;

// Forward function declaration
void setup_wifi();
void reconnect_mqtt();
void blink_red(int count, int length);
void blink_yellow(int count, int length);
void blink_green(int count, int length);
void get_sensor_data();
void sleep_now(int seconds);
bool publish_topic(const char *topic, const char *payload);

void setup() {

  pinMode(on_led, OUTPUT);
  digitalWrite(on_led, LOW);

  pinMode(red_led, OUTPUT);
  pinMode(yellow_led, OUTPUT);
  pinMode(green_led, OUTPUT);
  digitalWrite(red_led, LOW);
  digitalWrite(yellow_led, LOW);
  digitalWrite(green_led, LOW);

  Serial.begin(115200);

  setup_wifi();

  mqttClient.setServer(mqtt_server, mqtt_port);

  if (!bme.begin(0x76)) {
    Serial.println("Could not find a valid BME280 sensor, check wiring!");
    while (1)
      ;
  }

  if (!mqttClient.connected()) {
    reconnect_mqtt();
  }

  get_sensor_data();

  mqttClient.loop();

  Serial.println("Temperature: " + String(temperature) + " ºC");
  Serial.println("Humidity: " + String(humidity) + " %");
  Serial.println("Pressure: " + String(pressure) + " hPa");

  mqttClient.loop();

  bool pub_temp = publish_topic(topic_temperature, String(temperature).c_str());
  if (pub_temp == 1) {
    blink_green(1, 500);
  } else {
    blink_red(1, 500);
    blink_red(1, 1000);
  }

  bool pub_hum = publish_topic(topic_humidity, String(humidity).c_str());
  if (pub_hum == 1) {
    blink_green(1, 500);
  } else {
    blink_red(1, 500);
    blink_red(1, 1000);
  }

  bool pub_press = publish_topic(topic_pressure, String(pressure).c_str());
  if (pub_press == 1) {
    blink_green(1, 500);
  } else {
    blink_red(1, 500);
    blink_red(1, 1000);
  }

  Serial.println("MQTT disconnect");
  mqttClient.disconnect();

  delay(1000);
  Serial.print("Sleep for ");
  Serial.print(deep_sleep_timer);
  Serial.println(" seconds");
  sleep_now(deep_sleep_timer);
}

void loop() {}

void blink_red(int count, int length) {
  for (int i = 0; i < count; i++) {
    digitalWrite(red_led, HIGH);
    delay(length);
    digitalWrite(red_led, LOW);
    delay(length);
  }
}

void blink_yellow(int count, int length) {
  for (int i = 0; i < count; i++) {
    digitalWrite(yellow_led, HIGH);
    delay(length);
    digitalWrite(yellow_led, LOW);
    delay(length);
  }
}

void blink_green(int count, int length) {
  for (int i = 0; i < count; i++) {
    digitalWrite(green_led, HIGH);
    delay(length);
    digitalWrite(green_led, LOW);
    delay(length);
  }
}

void sleep_now(int seconds) { ESP.deepSleep(seconds * 1e6); }

void setup_wifi() {

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(wifi_ssid);

  WiFi.begin(wifi_ssid, wifi_password);

  timeNow = millis() / 1000;
  while (WiFi.status() != WL_CONNECTED) {
    blink_yellow(1, 50);
    Serial.print(".");
    timeDelta = millis() / 1000;
    if (timeDelta - timeNow >= connect_timeout) {
      Serial.println("Wifi connection timeout, going to sleep");
      sleep_now(no_wifi_sleep);
    }
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  blink_yellow(3, 500);
}

void reconnect_mqtt() {
  timeNow = millis() / 1000;
  while (!mqttClient.connected()) {
    Serial.print("Attempting MQTT connection...");

    if (mqttClient.connect("ESP8266Client", mqtt_user, mqtt_pass)) {
      Serial.println("connected");
    } else {
      timeDelta = millis() / 1000;
      if (timeDelta - timeNow >= connect_timeout) {
        Serial.println("MQTT connection timeout, sleep for " +
                       String(deep_sleep_timer) + " seconds");
        sleep_now(deep_sleep_timer);
      }
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(", try again in 5 seconds");
      blink_red(2, 100);
      delay(4600);
    }
  }
}

void get_sensor_data() {
  temperature = bme.readTemperature();
  humidity = bme.readHumidity();
  pressure = bme.readPressure() / 100.0F;
}

bool publish_topic(const char *topic, const char *payload) {
  Serial.println("Publish topic " + String(topic));
  bool publish = mqttClient.publish(topic, payload, true);
  return publish;
}
